# Maternia PHP Lenses

Lenses is a simple application, which was requested to be coded. Script calculates/predicts the next order date

# Requirements

  - PHP 7.x installed (no special modules are needed)
  - Web server (Apache)

# Settings
 - Settings are stored in `\sk\maternia\constants\Variables.php` 
 - `DATE_FORMAT` (string) - which format is expected.
 - `NOTIFY_N_DAYS_IN_ADVANCE` (int) - get notification date in advance?.
  
# Running the app

 - You can start app by running php interpreter using:
```sh
$ php index.php

# or using your web browser

```