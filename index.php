<?php

include_once 'autoload.php';

use sk\maternia\lenses\Lenses;
use sk\maternia\constants\Variables;
use sk\maternia\utils\Validator;


$orders = [
    '2015-04-01' => [
        [1, 2, '-2.00'],
        [1, 2, '-3.00'],
    ],
];

$orders2 = [
    '2014-10-01' => [
        [3, 2, '-1.50'],
        [3, 2, '-3.50'],
    ],
    '2015-01-01' => [
        [3, 2, '-1.50'],
        [3, 2, '-3.50'],
    ],
    '2015-04-15' => [
        [3, 1, '-1.50'],
        [3, 1, '-3.50'],
    ],
];

$orders3 = [
    '2014-08-01' => [
        [2, 2, '+0.50'],
    ],
];

$globalArr = [];
$globalArr[] = $orders;
$globalArr[] = $orders2;
$globalArr[] = $orders3;

$validator = new Validator();

foreach ($globalArr as $key => $orderArray) {

    try {
        $id = $validator->getOrderType($orderArray);

        $goodClass = Variables::getGood($id);
        if (class_exists($goodClass)) {
            $cls = new $goodClass;
            if ($cls instanceof Lenses) {
                $cls->setOrders($orderArray);
                echo "Next order for user `{$key}`"
                . " is planned for day: " . 
                    $cls->whenNextOrder()->format(Variables::DATE_FORMAT) . "<hr>\n";
            }
        }
    } catch (Exception $ex) {
        echo "Skipping for good `{$id}` user `{$key}` <br>\n";
        // to log or wherever
        echo "Full message: " . $ex->getMessage(). "<br><hr>\n";
    }
}