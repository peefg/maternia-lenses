<?php

namespace sk\maternia\orders;

class Orders
{
    private $arrOfOrders = [];
    
    /**
     * Adds order
     * 
     * @param \sk\maternia\orders\Order $order
     */
    public function addOrder(Order $order)
    {
        $this->arrOfOrders[] = $order;
    }
    
    /**
     * Retrieves order 
     * 
     * @return array list of orders
     */
    public function getOrders() : array 
    {
        return $this->arrOfOrders;
    }
    
}
