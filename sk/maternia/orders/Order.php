<?php

namespace sk\maternia\orders;

class Order
{
    private $dateLast;
    private $count;
    private $dateWillBe;
    private $howManyItems;
    
    /**
     * Creates order object
     * 
     * @param string $dateLast      date of last order (taken from key of order)
     * @param int $count            number of packages
     * @param int $howManyItems     total items
     */
    public function __construct(string $dateLast, int $count, int $howManyItems)
    {
        $this->count        = $count;
        $date               = new \DateTime($dateLast);
        $dateNew            = clone $date;
        $this->dateLast     = $date;
        $this->howManyItems = $howManyItems;

        $dateNew->modify("+ {$count} days");

        $this->dateWillBe   = $dateNew;
    }
    
    /**
     * Gets date of order
     * 
     * @return \DateTime
     */
    function getDateLast() : \DateTime
    {
        return $this->dateLast;
    }

    /**
     * Get count of packages
     * 
     * @return int
     */
    function getCount() : int
    {
        return $this->count;
    }

    /**
     * Gets new /calculated/ date
     * 
     * @return \DateTime
     */
    function getDateWillBe() : \DateTime
    {
        return $this->dateWillBe;
    }
    
    /**
     * Gets how many items have been in package
     * @return int
     */
    function getHowManyItems() : int
    {
        return $this->howManyItems;
    }



    
}
