<?php

namespace sk\maternia\utils;

class Validator
{
    /**
     * Very simple validation to get id of product
     * 
     * @param array $orders     list of input orders
     * 
     * @return int              id of prodcut
     * 
     * @throws \Exception       this has not been in an assignment, let's throw exception
     */
    public function getOrderType(array $orders) : int
    {
        foreach ($orders as $date => $inner) {
            foreach ($inner as $val) {
                $itemTypes[] = $val[0];
            }
        }

        $uniqueArr = array_unique($itemTypes);
        
        if (count($uniqueArr) > 1) {
            throw new \Exception("Don't know what to do, different orders... please reimplement");
        }
        
        return $uniqueArr[0];
    }
    
}
