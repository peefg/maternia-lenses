<?php

namespace sk\maternia\constants;

use sk\maternia\lenses\Biofinity180;
use sk\maternia\lenses\Biofinity90;
use sk\maternia\lenses\FocusDailies;

use sk\maternia\lenses\Lenses;

class Variables
{
    const DATE_FORMAT = 'Y-m-d';
    const NOTIFY_N_DAYS_IN_ADVANCE = 4;
    
    public static function getGood(int $goodId): string
    {
        $goods = [
            1 => Biofinity180::class, // Biofinity (6 lenses)
            2 => Biofinity90::class, // Biofinity (3 lenses)
            3 => FocusDailies::class, // Focus Dailies (30)
        ];

        if (!isset($goods[$goodId])) {
            throw new \Exception("That kind of good is not defined");
        }
        
        return $goods[$goodId];;
        
    }
    
}
