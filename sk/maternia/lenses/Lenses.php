<?php

namespace sk\maternia\lenses;

use sk\maternia\orders\Orders;
use sk\maternia\orders\Order;

use sk\maternia\constants\Variables;

abstract class Lenses
{
    
    protected $orders;
    
    /**
     * Calculate the date of the next possible order
     * Uses NOTIFY_N_DAYS_IN_ADVANCE to get N days back
     * and find first working day (so user can be notified on working day)
     * 
     * @return \DateTime    date of next order
     */
    public function whenNextOrder() : \DateTime
    {

        $maxOrderId = $this->getMaxOrderId();

        // when there are more orders, just calculate how often lenses had been replaced
        // and afterwards, based on date calculate new date
        if ($maxOrderId > 0) {

            $noItems = 0;

            for ($i = 0; $i <= $maxOrderId; $i++) {
                $order = $this->getOrderAtIndex($i);
                $noItems += $order->getHowManyItems();
            }

            $firstOrder = $this->getOrderAtIndex(0)->getDateLast();
            $lastOrder = $this->getOrderAtIndex($maxOrderId)->getDateLast();

            
            $diff = $firstOrder->diff($lastOrder);
            $onePack = $diff->days / $noItems;

            $itemsForLast = $this->getOrderAtIndex($maxOrderId)->getHowManyItems();

            $calcDays = floor($itemsForLast * $onePack) - Variables::NOTIFY_N_DAYS_IN_ADVANCE;

            return $this
                ->getOrderAtIndex($maxOrderId)
                ->getDateLast()
                ->modify('+' . $calcDays . 'days')
                ->modify('next weekday');
        } else {
            // very simple (it uses has not made more than 1 order) - just predict the date
            return $this->getOrderAtIndex(0)
                ->getDateWillBe()
                ->modify('- '. Variables::NOTIFY_N_DAYS_IN_ADVANCE . 'days')
                ->modify('next weekday');
        }
    }
    
    /**
     * Sets array of Order-s
     * 
     * @param array $ordersInput
     */
    function setOrders(array $ordersInput): void
    {

        $ordersOut = [];
        $orders = new Orders();
        foreach ($ordersInput as $when => $what) {
            $arr = [];

            foreach ($what as $item => $subs) {
                if (isset($arr[$subs[0]])) {
                    $arr[$subs[0]] += $subs[1];
                } else {
                    $arr[$subs[0]] = $subs[1];
                }
            }
            foreach ($arr as $cnt) {
                $sum = $this->lenses * $cnt * $this->days / 2;
                $orders->addOrder(
                    new Order($when, $sum, $cnt)
                );
            }
        }

        $this->orders = $orders->getOrders();
    }
    
    /**
     * Gets Order at specific index
     * 
     * @param int $index    index to look for
     * 
     * @return Order        Order at index
     * 
     * @throws Exception    no index found
     */
    private function getOrderAtIndex(int $index) : Order
    {
        if (!isset($this->orders[$index])) {
            throw new \Exception("Error, such index doesn't exist");
        }
        return $this->orders[$index];
    }
    
    /**
     * Gets lenses
     * @return int
     */
    public function getLenses(): int
    {
        return $this->lenses;
    }

    /**
     * Gets max order id
     * @return int
     */
    public function getMaxOrderId() : int
    {
        return count($this->orders) - 1;
    }
    
    /**
     * Gets days
     * @return int
     */
    function getDays(): int
    {
        return $this->days;
    }
    
}
